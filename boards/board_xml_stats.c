#ifndef _BOARD_XML_STATS_C
#define _BOARD_XML_STATS_C

#include "board_xml_common.c"

typedef struct {
    int dummy;
} board_xml_stats_layout_t;

static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_stats_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_stats_layout_t, stats, data);
    char *avrState;
    switch (board->s->sim->avr->state) {
        case cpu_Limbo:     avrState = nameof(cpu_Limbo); break;
        case cpu_Stopped:   avrState = nameof(cpu_Stopped); break;
        case cpu_Running:   avrState = nameof(cpu_Running); break;
        case cpu_Sleeping:  avrState = nameof(cpu_Sleeping); break;
        case cpu_Step:      avrState = nameof(cpu_Step); break;
        case cpu_StepDone:  avrState = nameof(cpu_StepDone); break;
        case cpu_Done:      avrState = nameof(cpu_Done); break;
        case cpu_Crashed:   avrState = nameof(cpu_Crashed); break;
        default: avrState = "unknown"; break;
    }
    glLTextF("S: %s  C: %llu\nT(s): %.4lfs  CT: %6.3lfus/%.3lfus (x%.3lf)\nT(f): %.4lfs  FPS: %.2lf",
             avrState, board->s->sim->avr->cycle,
             fmod(((double) (board->s->sim->currentTime - board->s->sim->startTime) / 1000.0 / 1000.0 / 1000.0), 3600.0),
             (double) board->s->sim->lastCycleDelta / 1000.0, (double) board->s->sim->targetCycleDelta / 1000.0,
             (double) board->s->sim->targetCycleDelta / (double) board->s->sim->lastCycleDelta,
             board->s->gui->currentFrameTime,
             board->s->gui->framesPerSecond);
}

static void board_xml_stats_layout_parse(board_xml_t *b, xmlNode *node, board_xml_layout_t *layout) {
    layout->onLayout = board_xml_stats_layout_onLayout;
//    board_xml_stats_layout_t *stats = layout->data = calloc(1, sizeof(board_xml_stats_layout_t)); // free
    
    
}



#endif // _BOARD_XML_STATS_C