#ifndef _BOARD_XML_LCD_C
#define _BOARD_XML_LCD_C

#include "board_xml_common.c"

#include <stdbool.h>

#include "../components/lcd.h"
#include "../components/lcd_gl.h"


typedef struct {
    lcd_t lcd;
    bool is8Bit;
    int width, height;
    board_xml_irq_t d0, d1, d2, d3, d4, d5, d6, d7, rs, en, rw;
} board_xml_lcd_t;

typedef struct {
    lcd_t *lcd;
    lcd_gl_colors_t theme;
} board_xml_lcd_layout_t;


static BOARD_XML_COMP_INIT(board_xml_lcd_comp_init, data, board, avr) {
    board_xml_data_cast(board_xml_lcd_t, lcd, data);
    lcd_init(&lcd->lcd, avr, lcd->width, lcd->height);
    if (!lcd->is8Bit) {
        board_xml_irq_setExternal(avr, lcd->d4, 0);
        board_xml_irq_setExternal(avr, lcd->d5, 0);
        board_xml_irq_setExternal(avr, lcd->d6, 0);
        board_xml_irq_setExternal(avr, lcd->d7, 0);
        lcd_connect4(&lcd->lcd,
                     board_xml_irq_get(avr, lcd->d4),
                     board_xml_irq_get(avr, lcd->d5),
                     board_xml_irq_get(avr, lcd->d6),
                     board_xml_irq_get(avr, lcd->d7),
                     board_xml_irq_get(avr, lcd->rs),
                     board_xml_irq_get(avr, lcd->en),
                     board_xml_irq_get(avr, lcd->rw));
    } else {
        board_xml_irq_setExternal(avr, lcd->d0, 0);
        board_xml_irq_setExternal(avr, lcd->d1, 0);
        board_xml_irq_setExternal(avr, lcd->d2, 0);
        board_xml_irq_setExternal(avr, lcd->d3, 0);
        board_xml_irq_setExternal(avr, lcd->d4, 0);
        board_xml_irq_setExternal(avr, lcd->d5, 0);
        board_xml_irq_setExternal(avr, lcd->d6, 0);
        board_xml_irq_setExternal(avr, lcd->d7, 0);
        lcd_connect8(&lcd->lcd,
                     board_xml_irq_get(avr, lcd->d0),
                     board_xml_irq_get(avr, lcd->d1),
                     board_xml_irq_get(avr, lcd->d2),
                     board_xml_irq_get(avr, lcd->d3),
                     board_xml_irq_get(avr, lcd->d4),
                     board_xml_irq_get(avr, lcd->d5),
                     board_xml_irq_get(avr, lcd->d6),
                     board_xml_irq_get(avr, lcd->d7),
                     board_xml_irq_get(avr, lcd->rs),
                     board_xml_irq_get(avr, lcd->en),
                     board_xml_irq_get(avr, lcd->rw));
    }
}

static BOARD_XML_LAYOUT_INIT(board_xml_lcd_layout_init, data, board) {
    board_xml_data_cast(board_xml_lcd_layout_t, lcd, data);
    lcd_doPrint = true;
    lcd_gl_init();
}
static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_lcd_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_lcd_layout_t, lcd, data);
    glLSimpleObject(lcd_gl_draw(lcd->lcd, lcd->theme), lcd_gl_getSize(lcd->lcd));
}




static void board_xml_lcd_parse(board_xml_t *board, xmlNode *node, board_xml_comp_t *comp) {
    comp->init = board_xml_lcd_comp_init;
    board_xml_lcd_t *lcd = comp->data = calloc(1, sizeof(board_xml_lcd_t)); // free
    lcd->width = (int) board_xml_int_parse(xmlUtilsFindAttrVal(node, "width"), 0);
    lcd->height = (int) board_xml_int_parse(xmlUtilsFindAttrVal(node, "height"), 0);
    
    xmlNode *optional = xmlUtilsFindChild(node, "optional");
    if ((lcd->is8Bit = optional != NULL)) {
        lcd->d0 = board_xml_irq_parse(xmlUtilsFindChild(optional, "d0"));
        lcd->d1 = board_xml_irq_parse(xmlUtilsFindChild(optional, "d1"));
        lcd->d2 = board_xml_irq_parse(xmlUtilsFindChild(optional, "d2"));
        lcd->d3 = board_xml_irq_parse(xmlUtilsFindChild(optional, "d3"));
    }
    lcd->d4 = board_xml_irq_parse(xmlUtilsFindChild(node, "d4"));
    lcd->d5 = board_xml_irq_parse(xmlUtilsFindChild(node, "d5"));
    lcd->d6 = board_xml_irq_parse(xmlUtilsFindChild(node, "d6"));
    lcd->d7 = board_xml_irq_parse(xmlUtilsFindChild(node, "d7"));
    lcd->rs = board_xml_irq_parse(xmlUtilsFindChild(node, "rs"));
    lcd->en = board_xml_irq_parse(xmlUtilsFindChild(node, "en"));
    lcd->rw = board_xml_irq_parse(xmlUtilsFindChild(node, "rw"));
}


static void board_xml_lcd_layout_parse(board_xml_t *board, xmlNode *node, board_xml_layout_t *layout) {
    layout->init = board_xml_lcd_layout_init;
    layout->onLayout = board_xml_lcd_layout_onLayout;
    board_xml_lcd_layout_t *lcd = layout->data = calloc(1, sizeof(board_xml_lcd_layout_t)); // free
    lcd->lcd = &board_xml_findCompDataByAttr(board_xml_lcd_t, board, xmlUtilsFindAttrVal(node, "id"))->lcd;

    const char *themeName = board_xml_str_parse(xmlUtilsFindAttrVal(node, "theme"), "green");
    if (!strcmp(themeName, "green"))
        lcd->theme = (lcd_gl_colors_t) {.bg = 0x00AA00FF, .char_bg = 0x00000033, .shadow = 0x00330077, .text = 0x001100FF};
    else if (!strcmp(themeName, "blue"))
        lcd->theme = (lcd_gl_colors_t) {.bg = 0x5555FFFF, .char_bg = 0x0000AA11, .shadow = 0xFFFFFF22, .text = 0xFFFFFFBB};
    else if (!strcmp(themeName, "dark white"))
        lcd->theme = (lcd_gl_colors_t) {.bg = 0x000000FF, .char_bg = 0xCCCCCC22, .shadow = 0xFFFFFF55, .text = 0xFFFFFFFF};
    else if (!strcmp(themeName, "dark red"))
        lcd->theme = (lcd_gl_colors_t) {.bg = 0x000000FF, .char_bg = 0xCC000022, .shadow = 0xFF000055, .text = 0xFF0000FF};
    else if (!strcmp(themeName, "dark green"))
        lcd->theme = (lcd_gl_colors_t) {.bg = 0x002200FF, .char_bg = 0x00CC0044, .shadow = 0x00FF0055, .text = 0x00FF00FF};
}



#endif // _BOARD_XML_LCD_C