cmake_minimum_required(VERSION 3.9)

project(AVRSimV2 VERSION 2.3.5)

set(INCLUDE_DIRS)
set(LIBRARY_DIRS)
set(BIN_FILES)


if (WIN32)
    set(AVRSIMV2_INSTALL_DIR ${CMAKE_SOURCE_DIR}/installs/avrsimv2-win-${CMAKE_PROJECT_VERSION})
    
    list(APPEND INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/win/include)
    list(APPEND LIBRARY_DIRS ${CMAKE_SOURCE_DIR}/win/lib)
    file(GLOB WIN_DLLS ${CMAKE_SOURCE_DIR}/win/bin/*)
    list(APPEND BIN_FILES ${WIN_DLLS})

    find_package(OpenGL REQUIRED)

    set(L_MISC wsock32 ws2_32)
    set(L_MATH)
    set(L_PTHREAD pthreadVC2)
    set(L_SIMAVR simavr)
    set(L_ELF)
    set(L_OPENGL OpenGL::GL)
    set(L_GLEW glew32)
    set(L_GLFW glfw3dll)
    set(L_FREETYPE freetype)
    set(L_DL dl)
    set(L_XML libxml2_a)
else ()
    set(AVRSIMV2_INSTALL_DIR ${CMAKE_SOURCE_DIR}/installs/avrsimv2-${CMAKE_PROJECT_VERSION})
    
    list(APPEND INCLUDE_DIRS /usr/include/freetype2)
    list(APPEND INCLUDE_DIRS /usr/include/libxml2)

    set(OpenGL_GL_PREFERENCE GLVND)
    find_package(OpenGL REQUIRED)

    set(L_MISC)
    set(L_MATH m)
    set(L_PTHREAD pthread)
    set(L_SIMAVR simavr)
    set(L_ELF elf)
    set(L_OPENGL OpenGL::GL)
    set(L_GLEW GLEW)
    set(L_GLFW glfw)
    set(L_FREETYPE freetype)
    set(L_DL dl)
    set(L_XML xml2)
endif ()

if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

function(target_binaries TARGET)
    if (ARGN)
        add_custom_command(TARGET ${TARGET} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${ARGN}
                $<TARGET_FILE_DIR:${TARGET}>
                DEPENDS ${ARGN})
    endif ()
endfunction()
function(target_resources TARGET DIR DEST)
    add_custom_command(TARGET ${TARGET} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${DIR}
            $<TARGET_FILE_DIR:${TARGET}>/${DEST})
endfunction()


add_subdirectory(common)
add_subdirectory(components)
add_subdirectory(boards)


add_executable(avrsimv2-bin main.c)
set_target_properties(avrsimv2-bin PROPERTIES OUTPUT_NAME avrsimv2)
add_dependencies(avrsimv2-bin boards)
target_include_directories(avrsimv2-bin PRIVATE ${INCLUDE_DIRS})
target_link_directories(avrsimv2-bin PRIVATE ${LIBRARY_DIRS})
target_link_libraries(avrsimv2-bin avrsimv2)
target_binaries(avrsimv2-bin ${BIN_FILES})
target_resources(avrsimv2-bin ${CMAKE_SOURCE_DIR}/res res)



install(TARGETS avrsimv2-bin RUNTIME DESTINATION ${AVRSIMV2_INSTALL_DIR})
install(FILES ${BIN_FILES} DESTINATION ${AVRSIMV2_INSTALL_DIR})
install(DIRECTORY ${CMAKE_SOURCE_DIR}/res DESTINATION ${AVRSIMV2_INSTALL_DIR})

