#ifndef _BOARD_COMMON_H
#define _BOARD_COMMON_H

#include <stdbool.h>
#include <simavr/sim_avr.h>

typedef struct board_t__ board_t;

#include "avrsimv2.h"
#include "gl_utils.h"
#include "log.h"

#define BOARD_FUNC_AVR_INIT(name, board, avr)                          void name(board_t *board, avr_t *avr)
#define BOARD_FUNC_GUI_INIT(name, board)                               void name(board_t *board)
#define BOARD_FUNC_GUI_GETSIZE(name, board)                            GLvec2f name(board_t *board)
#define BOARD_FUNC_GUI_DRAW(name, board)                               void name(board_t *board)
#define BOARD_FUNC_GUI_ONKEY(name, board, key, scancode, action, mods) void name(board_t *board, int key, int scancode, int action, int mods)

#define BOARD_FUNC_CLOSE(name, board)                                  void name(board_t *board)


typedef BOARD_FUNC_AVR_INIT   (board_func_avr_init_t, , );
typedef BOARD_FUNC_GUI_INIT   (board_func_gui_init_t, );
typedef BOARD_FUNC_GUI_GETSIZE(board_func_gui_getSize_t, );
typedef BOARD_FUNC_GUI_DRAW   (board_func_gui_draw_t, );
typedef BOARD_FUNC_GUI_ONKEY  (board_func_gui_onKey_t, , , , , );
typedef BOARD_FUNC_CLOSE      (board_func_close_t, );

struct board_t__ {
    avrsimv2_t *s;
    logger_t logger;
    struct {
        board_func_avr_init_t *init;
    } avr;
    struct {
        board_func_gui_init_t *init;
        board_func_gui_getSize_t *getSize;
        board_func_gui_draw_t *draw;
        board_func_gui_onKey_t *onKey;
    } gui;
    board_func_close_t *close;
    void *dl_handle;
    void *custom;
};

#define BOARD_LOAD_F_NAME board_load_f__
#define BOARD_LOAD_F(board, argc, argv) void BOARD_LOAD_F_NAME(board_t *board, int argc, char *argv[])

#endif //_BOARD_COMMON_H
