#include "opts.h"


bool opts_debugOut = false;



static int handleArg(int *argc, char ***argv, opt_arg_t arg) {
    char *str = (*argv)[0];
    if (str[0] == '-') return OPT_ERROR;
    if (opts_debugOut) printf("arg: \"%s\"\n", str);
    if (arg.callback != NULL) {
        int callbackCode;
        if ((callbackCode = arg.callback(argc, argv, arg))) return callbackCode;
    }
    switch (arg.type) {
        case OPT_ARG_STRING:        *((             char **) arg.target) = str; break;
        case OPT_ARG_LONG:          *((              long *) arg.target) = strtol(str, NULL, arg.base); break;
        case OPT_ARG_LONGLONG:      *((         long long *) arg.target) = strtoll(str, NULL, arg.base); break;
        case OPT_ARG_ULONG:         *((     unsigned long *) arg.target) = strtoul(str, NULL, arg.base); break;
        case OPT_ARG_ULONGLONG:     *((unsigned long long *) arg.target) = strtoull(str, NULL, arg.base); break;
        case OPT_ARG_FLOAT:         *((             float *) arg.target) = strtof(str, NULL); break;
        case OPT_ARG_DOUBLE:        *((            double *) arg.target) = strtod(str, NULL); break;
        case OPT_ARG_LONGDOUBLE:    *((       long double *) arg.target) = strtold(str, NULL); break;
    }
    (*argc)--;
    (*argv)++;
    return OPT_SUCCESS;
}

static int handleOpt(int *argc, char ***argv, bool isLong, opt_t opt) {
    char tmp[] = {opt.sname, '\0'};
    char *name = isLong ? opt.lname : tmp;
    if (opts_debugOut) printf("opt: %s >>>>\n", name);
    if (opt.counter != NULL) (*opt.counter)++;
    if (opt.flag != NULL) *opt.flag = opt.flagVal;
    if (opt.callback != NULL) {
        int callbackCode;
        if ((callbackCode = opt.callback(argc, argv, opt))) return callbackCode;
    }
    int i = 0;
    for (; i < opt.minArgs; i++) {
        if (*argc > 0 && handleArg(argc, argv, opt.args[i]) != OPT_ERROR) continue;
        fprintf(stderr, "Option '%s' requires at least %i argument(s), but got %i\n", name, opt.minArgs, i);
        return OPT_ERROR;
    }
    while (i < opt.maxArgs && *argc > 0 && handleArg(argc, argv, opt.args[i]) == OPT_SUCCESS) i++;
    if (opts_debugOut) printf("<<<<\n");
    return OPT_SUCCESS;
}


int opts_parseSingle(int *argc, char ***argv, int optc, opt_t *opts) {
    if (*argc <= 0) return OPT_ERROR;
    char *c = (*argv)[0];
    if (c[0] != '-') return OPT_ARG;
    c++;
    int opti;
    if (c[0] == '-') {
        c++;
        for (opti = 0; opti < optc; opti++) {
            if (opts[opti].lname != NULL && !strcmp(c, opts[opti].lname)) {
                (*argc)--;
                (*argv)++;
                if (handleOpt(argc, argv, true, opts[opti]) == OPT_ERROR) return OPT_ERROR;
                return opti;
            }
        }
        goto lerror;
    } else {
        if (c[0] == '\0') return OPT_ERROR;
        while (c[1] != '\0') {
            for (opti = 0; opti < optc; opti++) {
                if (c[0] == opts[opti].sname) {
                    int zero = 0;
                    if (handleOpt(&zero, NULL, false, opts[opti]) == OPT_ERROR) return OPT_ERROR;
                    goto found;
                }
            }
            goto serror;
            found:
            c++;
        }
        for (opti = 0; opti < optc; opti++) {
            if (c[0] == opts[opti].sname) {
                (*argc)--;
                (*argv)++;
                if (handleOpt(argc, argv, false, opts[opti]) == OPT_ERROR) return OPT_ERROR;
                return opti;
            }
        }
    }
    lerror:
    fprintf(stderr, "Unknown option '--%s'\n", c);
    return OPT_ERROR;
    serror:
    fprintf(stderr, "Unknown option '-%c'\n", c[0]);
    return OPT_ERROR;
}

int opts_parse(int *argc, char ***argv, opts_t opts) {
    int argi = 0;
    while ((*argc) > 0) {
        switch (opts_parseSingle(argc, argv, opts.optc, opts.opts)) {
            case OPT_ERROR: return OPT_ERROR;
            case OPT_ARG: {
                if (argi >= opts.maxArgs) {
                    fprintf(stderr, "Maximal %i argument(s) allowed\n", opts.maxArgs);
                    return OPT_ERROR;
                }
                handleArg(argc, argv, opts.args[argi++]);
            } break;
            default: break;
        }
    }
    if (argi < opts.minArgs) {
        fprintf(stderr, "Required at least %i argument(s), but got %i\n", opts.minArgs, argi);
        return OPT_ERROR;
    }
    return OPT_SUCCESS;
}



#define OPTS_C_ESCAPE(v)    v //"\e["v"m"
#define OPTS_C_RESET        //OPTS_C_ESCAPE("0")
#define OPTS_C_OPT(v)       v //OPTS_C_ESCAPE("32") v OPTS_C_RESET
#define OPTS_C_ARG(v)       v //OPTS_C_ESCAPE("36") v OPTS_C_RESET

static void printArgName(int index, opt_arg_t arg, bool optional, int align) {
    if (optional) printf("[");
    if (align > 3) {
        if (arg.name != NULL) printf(OPTS_C_ARG("%-*s"), align, arg.name);
        else printf(OPTS_C_ARG("arg%-*i"), align - 3, index + 1);
    } else {
        if (arg.name != NULL) printf(OPTS_C_ARG("%s"), arg.name);
        else printf(OPTS_C_ARG("arg%i"), index + 1);
    }
    if (optional) printf("]");
}


void opts_printUsage(opts_t opts) {
    printf("\nUsage:");
    if (opts.optc > 0) printf(" [OPTION]...");
    int i = 0;
    for (; i < opts.minArgs; i++) {
        printf(" ");
        printArgName(i, opts.args[i], false, 0);
    }
    for (; i < opts.maxArgs; i++) {
        printf(" ");
        printArgName(i, opts.args[i], true, 0);
    }
    printf("\n\n");

    for (int j = 0; j < opts.maxArgs; j++) {
        if (opts.args[j].description == NULL) continue;
        printf("    ");
        printArgName(j, opts.args[j], false, 20);
        printf("    %s\n\n", opts.args[j].description);
    }

    printf("  OPTIONS\n\n");
    int syntaxSize = 80;
    char **optSyntax = calloc(opts.optc, sizeof(char *));
    int maxSyntaxLen = 0;
    for (i = 0; i < opts.optc; i++) {
        int s = 0;
        optSyntax[i] = malloc(syntaxSize);
#define OPTS_APPEND_SYNTAX(f, ...) s += snprintf(optSyntax[i] + s, syntaxSize - s, f, ##__VA_ARGS__)
        if (opts.opts[i].sname != 0) OPTS_APPEND_SYNTAX(" "OPTS_C_OPT("-%c"), opts.opts[i].sname);
        if (opts.opts[i].lname != NULL) OPTS_APPEND_SYNTAX(" "OPTS_C_OPT("--%s"), opts.opts[i].lname);
        int j = 0;
        for (; j < opts.opts[i].minArgs; j++) {
            if (opts.opts[i].args[j].name != NULL) OPTS_APPEND_SYNTAX(" "OPTS_C_ARG("%s")"", opts.opts[i].args[j].name);
            else OPTS_APPEND_SYNTAX(" "OPTS_C_ARG("arg%i")"", j + 1);
        }
        for (; j < opts.opts[i].maxArgs; j++) {
            if (opts.opts[i].args[j].name != NULL) OPTS_APPEND_SYNTAX(" ["OPTS_C_ARG("%s")"]", opts.opts[i].args[j].name);
            else OPTS_APPEND_SYNTAX(" ["OPTS_C_ARG("arg%i")"]", j + 1);
        }
#undef OPTS_APPEND_SYNTAX
        if (s > maxSyntaxLen) maxSyntaxLen = s;
    }
    for (i = 0; i < opts.optc; i++) {
        printf("    %-*s    ", maxSyntaxLen, optSyntax[i]);
        if (opts.opts[i].description != NULL) printf("%s", opts.opts[i].description);
        printf("\n\n");
        free(optSyntax[i]);
        for (int j = 0; j < opts.opts[i].maxArgs; j++) {
            if (opts.opts[i].args[j].description == NULL) continue;
            printf("    %-*s      ", maxSyntaxLen, "");
            printArgName(j, opts.opts[i].args[j], false, 10);
            printf("    %s\n", opts.opts[i].args[j].description);
        }
        if (opts.opts[i].maxArgs > 0) printf("\n");
    }
    free(optSyntax);
    printf("\n");
}
