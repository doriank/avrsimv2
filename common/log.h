#ifndef _LOG_H
#define _LOG_H

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>

typedef struct logger_t__ logger_t;

#include "compat.h"


#define CON_ENABLED 1

#if IS_WIN
#define CON_ESCAPE "\x1b"
#else
#define CON_ESCAPE "\e"
#endif

#if CON_ENABLED
#define CON_C(c) CON_ESCAPE"["c"m"
#else
#define CON_C(c)
#endif

#define CON_RESET               CON_C("0")
#define CON_BLACK               CON_C("30")
#define CON_RED                 CON_C("31")
#define CON_GREEN               CON_C("32")
#define CON_YELLOW              CON_C("33")
#define CON_BLUE                CON_C("34")
#define CON_MAGENTA             CON_C("35")
#define CON_CYAN                CON_C("36")
#define CON_WHITE               CON_C("37")
#define CON_B_BLACK             CON_C("90")
#define CON_B_RED               CON_C("91")
#define CON_B_GREEN             CON_C("92")
#define CON_B_YELLOW            CON_C("93")
#define CON_B_BLUE              CON_C("94")
#define CON_B_MAGENTA           CON_C("95")
#define CON_B_CYAN              CON_C("96")
#define CON_B_WHITE             CON_C("97")
#define CON_BG_BLACK            CON_C("40")
#define CON_BG_RED              CON_C("41")
#define CON_BG_GREEN            CON_C("42")
#define CON_BG_YELLOW           CON_C("43")
#define CON_BG_BLUE             CON_C("44")
#define CON_BG_MAGENTA          CON_C("45")
#define CON_BG_CYAN             CON_C("46")
#define CON_BG_WHITE            CON_C("47")
#define CON_BG_B_BLACK          CON_C("100")
#define CON_BG_B_RED            CON_C("101")
#define CON_BG_B_GREEN          CON_C("102")
#define CON_BG_B_YELLOW         CON_C("103")
#define CON_BG_B_BLUE           CON_C("104")
#define CON_BG_B_MAGENTA        CON_C("105")
#define CON_BG_B_CYAN           CON_C("106")
#define CON_BG_B_WHITE          CON_C("107")

/*
Black	            30	40
Red	                31	41
Green	            32	42
Yellow	            33	43
Blue	            34	44
Magenta	            35	45
Cyan	            36	46
White	            37	47
Bright Black (Gray)	90	100
Bright Red	        91	101
Bright Green	    92	102
Bright Yellow	    93	103
Bright Blue	        94	104
Bright Magenta	    95	105
Bright Cyan	        96	106
Bright White	    97	107
*/



enum {
    L_LEVEL_NONE = 0,
    L_LEVEL_ERROR,
    L_LEVEL_WARNING,
    L_LEVEL_INFO,
    L_LEVEL_DEBUG,
    L_LEVEL_TRACE
};

struct logger_t__ {
    int level;
    bool color;
};

extern logger_t logger;


#define L_TRACE(msg, ...)   do { if (!logger.color) L_TRACE_NC(msg, ##__VA_ARGS__);   else if (logger.level >= L_LEVEL_TRACE) printf("TRACE: "msg" (%s)\n", ##__VA_ARGS__, __func__); } while (0)
#define L_DEBUG(msg, ...)   do { if (!logger.color) L_DEBUG_NC(msg, ##__VA_ARGS__);   else if (logger.level >= L_LEVEL_DEBUG) printf(CON_B_CYAN"DEBUG: "msg""CON_RESET"\n", ##__VA_ARGS__); } while (0)
#define L_INFO(msg, ...)    do { if (!logger.color) L_INFO_NC(msg, ##__VA_ARGS__);    else if (logger.level >= L_LEVEL_INFO) printf(CON_B_BLUE"INFO: "msg""CON_RESET"\n", ##__VA_ARGS__); } while (0)
#define L_WARNING(msg, ...) do { if (!logger.color) L_WARNING_NC(msg, ##__VA_ARGS__); else if (logger.level >= L_LEVEL_WARNING) printf(CON_B_YELLOW"WARNING: "msg""CON_RESET"\n", ##__VA_ARGS__); } while (0)
#define L_ERROR(msg, ...)   do { if (!logger.color) L_ERROR_NC(msg, ##__VA_ARGS__);   else if (logger.level >= L_LEVEL_ERROR) fprintf(stderr, CON_B_RED"ERROR: "msg""CON_RESET"\n", ##__VA_ARGS__); } while (0)

#define L_TRACE_NC(msg, ...)   do { if (logger.level >= L_LEVEL_TRACE) printf("TRACE: "msg" (%s)\n", ##__VA_ARGS__, __func__); } while (0)
#define L_DEBUG_NC(msg, ...)   do { if (logger.level >= L_LEVEL_DEBUG) printf("DEBUG: "msg"\n", ##__VA_ARGS__); } while (0)
#define L_INFO_NC(msg, ...)    do { if (logger.level >= L_LEVEL_INFO) printf("INFO: "msg"\n", ##__VA_ARGS__); } while (0)
#define L_WARNING_NC(msg, ...) do { if (logger.level >= L_LEVEL_WARNING) printf("WARNING: "msg"\n", ##__VA_ARGS__); } while (0)
#define L_ERROR_NC(msg, ...)   do { if (logger.level >= L_LEVEL_ERROR) fprintf(stderr, "ERROR: "msg"\n", ##__VA_ARGS__); } while (0)


#define nameof(x) #x


char *strallocf(size_t *outLength, const char *format, ...);
char *vstrallocf(size_t *outLength, const char *format, va_list args);


#endif //_LOG_H
