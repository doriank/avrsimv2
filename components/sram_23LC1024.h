#ifndef _SRAM_23LC1024_H
#define _SRAM_23LC1024_H

#include "spi.h"

#include <stdint.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>


typedef enum {
    SRAM_23LC1024_STATE_INSTR,
    SRAM_23LC1024_STATE_ADDR1,
    SRAM_23LC1024_STATE_ADDR2,
    SRAM_23LC1024_STATE_ADDR3,
    SRAM_23LC1024_STATE_OP,
    SRAM_23LC1024_STATE_IDLE
} sram_23LC1024_state;

#define SRAM_23LC1024_INSTR_READ    0x03
#define SRAM_23LC1024_INSTR_WRITE   0x02
#define SRAM_23LC1024_INSTR_EDIO    0x3B
#define SRAM_23LC1024_INSTR_EQIO    0x38
#define SRAM_23LC1024_INSTR_RSTIO   0xFF
#define SRAM_23LC1024_INSTR_RDMR    0x05
#define SRAM_23LC1024_INSTR_WRMR    0x01

#define SRAM_23LC1024_MODE_BYTE         0x00
#define SRAM_23LC1024_MODE_PAGE         0x80
#define SRAM_23LC1024_MODE_SEQUENTIAL   0x40

#define SRAM_23LC1024_PAGE_MASK ((uint32_t) 0x1F)
//#define SRAM_23LC1024_SIZE 0x01FFFF
#define SRAM_23LC1024_SIZE 0x020000

typedef struct {
    avr_t *avr;
    avr_irq_t *irq;
    const char *name;
    spi_slave_t spi;
    sram_23LC1024_state state;
    uint8_t instr;
    uint8_t mode;
    uint32_t addr;
    uint8_t memory[SRAM_23LC1024_SIZE];
} sram_23LC1024_t;

void sram_23LC1024_init(sram_23LC1024_t *l, avr_t *avr);

#if SPI_SLAVE_BITBANG

enum {
    IRQ_SRAM_23LC1024_CS = 0,
    IRQ_SRAM_23LC1024_SO,
    IRQ_SRAM_23LC1024_SIO1 = IRQ_SRAM_23LC1024_SO,
    IRQ_SRAM_23LC1024_SIO2,
    IRQ_SRAM_23LC1024_SIO3,
    IRQ_SRAM_23LC1024_SCK,
    IRQ_SRAM_23LC1024_SI,
    IRQ_SRAM_23LC1024_SIO0 = IRQ_SRAM_23LC1024_SI,
    IRQ_SRAM_23LC1024_COUNT
};
void sram_23LC1024_connect_spi(sram_23LC1024_t *l, avr_irq_t *cs, avr_irq_t *so, avr_irq_t *sck, avr_irq_t *si);

#else

enum {
    IRQ_SRAM_23LC1024_CS = 0,
    IRQ_SRAM_23LC1024_MOSI,
    IRQ_SRAM_23LC1024_MISO,
    IRQ_SRAM_23LC1024_COUNT
};
void sram_23LC1024_connect_spi(sram_23LC1024_t *l, avr_irq_t *cs, avr_irq_t *mosi, avr_irq_t *miso);

#endif

#endif //_SRAM_23LC1024_H