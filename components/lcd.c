#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <simavr/avr_ioport.h>

#include "lcd.h"

#include "../common/log.h"



enum {
    LCD_FLAG_F = 0,          // 1: 5x10 Font, 0: 5x7 Font
    LCD_FLAG_N,              // 1: 2/4-lines Display, 0: 1-line Display,
    LCD_FLAG_D_L,            // 1: 4-Bit Interface, 0: 8-Bit Interface
    LCD_FLAG_R_L,            // 1: Shift right, 0: shift left
    LCD_FLAG_S_C,            // 1: Display shift, 0: Cursor move
    LCD_FLAG_B,              // 1: Cursor Blink
    LCD_FLAG_C,              // 1: Cursor on
    LCD_FLAG_D,              // 1: Set Entire Display memory (for clear)
    LCD_FLAG_S,              // 1: Follow display shift
    LCD_FLAG_I_D,            // 0: Increment, 1: Decrement

    // internal flags
    LCD_FLAG_LOWNIBBLE,      // 1: 4 bits mode, write/read low nibble
    LCD_FLAG_BUSY,           // 1: Busy between instruction, 0: ready
    LCD_FLAG_REENTRANT,      // 1: Do not update pins

    LCD_FLAG_DIRTY           // 1: needs redisplay
};



bool lcd_doPrint = true;

void lcd_print(lcd_t *b, FILE *stream) {
    const uint8_t offset[] = {0, 0x40, 0x20, 0x60}; // TODO expand for more than 4 rows
    char *vert = malloc(sizeof(char) * (b->cols + 2 + 1));
    for (size_t i = 0; i < b->cols + 2u; i++) vert[i] = '-';
    vert[b->cols + 2] = '\0';
    fprintf(stream, "%s\n", vert);
    for (int i = 0; i < b->rows; i++) {
        fprintf(stream, "| ");
        fwrite(b->vram + offset[i], 1, b->cols, stream);
        fprintf(stream, " |\n");
    }
    fprintf(stream, "%s\n", vert);
    free(vert);
}


static inline int lcd_set_flag(lcd_t *b, uint16_t bit, bool val) {
    bool old = b->flags & (1u << bit);
    b->flags = (b->flags & ~(1u << bit)) | (val ? (1u << bit) : 0);
    return old != 0;
}

static inline bool lcd_get_flag(lcd_t *b, uint16_t bit) {
    return (b->flags & (1u << bit)) != 0;
}


static void lcd_reset_cursor(lcd_t *b) {
    b->cursor = 0;
    lcd_set_flag(b, LCD_FLAG_DIRTY, 1);
    avr_raise_irq(b->irq + IRQ_LCD_ADDR, b->cursor);
}

static void lcd_clear_screen(lcd_t *b) {
    memset(b->vram, ' ', lcd_ddram_size);
    lcd_set_flag(b, LCD_FLAG_DIRTY, 1);
}



static avr_cycle_count_t lcd_busy_timer(struct avr_t *avr, avr_cycle_count_t when, void *param) {
    lcd_t *b = (lcd_t *) param;
    lcd_set_flag(b, LCD_FLAG_BUSY, 0);
    avr_raise_irq(b->irq + IRQ_LCD_BUSY, 0);
    return 0;
}

static void lcd_move_cursor(lcd_t *b) {
    // TODO cursor movement may not be accurate
    if (b->cursor < lcd_ddram_size) {
        if (!lcd_get_flag(b, LCD_FLAG_I_D)) {
            if (b->cursor < lcd_ddram_size - 1) b->cursor++;
        } else if (b->cursor > 0) b->cursor--;
    } else if (b->cursor < lcd_ddram_size + lcd_cgram_size - 1) b->cursor++;
    lcd_set_flag(b, LCD_FLAG_DIRTY, 1);
    avr_raise_irq(b->irq + IRQ_LCD_ADDR, b->cursor);
}

static uint32_t lcd_write_data(lcd_t *b) {
    uint32_t delay = 37;
    b->vram[b->cursor] = b->datapins;
    if (lcd_get_flag(b, LCD_FLAG_S_C)) {
        // TODO display shift
        L_WARNING("LCD: display shift is not supported");
    } else lcd_move_cursor(b);
    lcd_set_flag(b, LCD_FLAG_DIRTY, 1);
    if (lcd_doPrint && b->cursor < lcd_ddram_size) lcd_print(b, stdout);
    return delay;
}

static uint32_t lcd_write_command(lcd_t *b) {
    uint32_t delay = 37;
    uint8_t top = 7;
    while (top)
        if (b->datapins & (1u << top)) break;
        else top--;
    if (top == 0 && !(b->datapins & 1u)) top = 8;

    switch (top) {
        case 7: // Set DDRAM address // 1 ADD ADD ADD ADD ADD ADD ADD
            b->cursor = b->datapins & 0x7Fu;
            break;
        case 6: // Set CGRAM address // 0 1 ADD ADD ADD ADD ADD ADD ADD
            b->cursor = lcd_ddram_size + (b->datapins & 0x3Fu);
            break;
        case 5: { // Function set // 0 0 1 DL N F x x
            int four = !lcd_get_flag(b, LCD_FLAG_D_L);
            lcd_set_flag(b, LCD_FLAG_D_L, b->datapins & 16u);
            lcd_set_flag(b, LCD_FLAG_N, b->datapins & 8u);
            lcd_set_flag(b, LCD_FLAG_F, b->datapins & 4u);
            if (!four && !lcd_get_flag(b, LCD_FLAG_D_L)) lcd_set_flag(b, LCD_FLAG_LOWNIBBLE, 0);
            break;
        }
        case 4: // Cursor display shift // 0 0 0 1 S/C R/L x x
            lcd_set_flag(b, LCD_FLAG_S_C, b->datapins & 8u);
            lcd_set_flag(b, LCD_FLAG_R_L, b->datapins & 4u);
            break;
        case 3: // Display on/off control // 0 0 0 0 1 D C B
            lcd_set_flag(b, LCD_FLAG_D, b->datapins & 4u);
            lcd_set_flag(b, LCD_FLAG_C, b->datapins & 2u);
            lcd_set_flag(b, LCD_FLAG_B, b->datapins & 1u);
            lcd_set_flag(b, LCD_FLAG_DIRTY, 1);
            break;
        case 2: // Entry mode set // 0 0 0 0 0 1 I/D S
            lcd_set_flag(b, LCD_FLAG_I_D, b->datapins & 2u);
            lcd_set_flag(b, LCD_FLAG_S, b->datapins & 1u);
            break;
        case 1: // Return home // 0 0 0 0 0 0 1 x
            lcd_reset_cursor(b);
            delay = 1520;
            break;
        case 0: // Clear display // 0 0 0 0 0 0 0 1
            lcd_clear_screen(b);
            lcd_reset_cursor(b);
            delay = 1520;
            break;
        default:
            L_WARNING("LCD: unexpected command (0x%x)", b->datapins);
            break;
    }
    return delay;
}

static uint32_t lcd_process_write(lcd_t *b) {
    uint32_t delay = 0;
    int four = !lcd_get_flag(b, LCD_FLAG_D_L);
    int comp = four && lcd_get_flag(b, LCD_FLAG_LOWNIBBLE);
    int write = 0;

    if (four) {
        b->datapins = comp
                ? (b->datapins & 0xF0u) | ((uint8_t) (b->pinstate >> (uint8_t)  IRQ_LCD_D4     ) & 0xFu )
                : (b->datapins & 0xFu ) | ((uint8_t) (b->pinstate >> (uint8_t) (IRQ_LCD_D4 - 4)) & 0xF0u);
        write = comp;
        b->flags ^= (1u << (uint8_t) LCD_FLAG_LOWNIBBLE);
    } else {
        b->datapins = (uint8_t) (b->pinstate >> (uint8_t) IRQ_LCD_D0) & 0xffu;
        write++;
    }
    avr_raise_irq(b->irq + IRQ_LCD_DATA_IN, b->datapins);
    
    
    if (write) {
        if (lcd_get_flag(b, LCD_FLAG_BUSY)) L_WARNING("LCD: command 0x%02x write when still BUSY", b->datapins);
        delay = b->pinstate & (1u << (uint8_t) IRQ_LCD_RS)
                ? lcd_write_data(b)
                : lcd_write_command(b);
    }
    return delay;
}

static uint32_t lcd_process_read(lcd_t *b) {
    uint32_t delay = 0;
    bool four = !lcd_get_flag(b, LCD_FLAG_D_L);
    
    if (four && lcd_get_flag(b, LCD_FLAG_LOWNIBBLE)) {
        b->readpins <<= 4u;
        lcd_set_flag(b, LCD_FLAG_LOWNIBBLE, 0);
    } else {
        if (b->pinstate & (1u << (uint8_t) IRQ_LCD_RS)) { // data
            delay = 37;
            b->readpins = b->vram[b->cursor];
            lcd_move_cursor(b);
        } else { // busy flag
            delay = 0;
            b->readpins = lcd_get_flag(b, LCD_FLAG_BUSY) ? 0x80 : 0;
            // this may be incorrect, but isn't usually used anyway
            b->readpins |= b->cursor < lcd_ddram_size ? b->cursor : b->cursor - lcd_ddram_size;

            lcd_set_flag(b, LCD_FLAG_BUSY, 0);
            avr_raise_irq(b->irq + IRQ_LCD_BUSY, 0);
            avr_cycle_timer_cancel(b->avr, lcd_busy_timer, b);
        }
        avr_raise_irq(b->irq + IRQ_LCD_DATA_OUT, b->readpins);
        
        if (four) lcd_set_flag(b, LCD_FLAG_LOWNIBBLE, 1);
    }

    for (uint8_t i = four ? 4 : 0; i < 8; i++) avr_raise_irq(b->irq + IRQ_LCD_D0 + i, (uint8_t) (b->readpins >> i) & 1u);
    return delay;
}

static avr_cycle_count_t lcd_process_input(struct avr_t *avr, avr_cycle_count_t when, void *param) {
    lcd_t *b = (lcd_t *) param;
    lcd_set_flag(b, LCD_FLAG_REENTRANT, 1);
    
    uint32_t delay = b->pinstate & (1u << (uint8_t) IRQ_LCD_RW)
            ? lcd_process_read(b)
            : lcd_process_write(b);

    if (delay) {
        lcd_set_flag(b, LCD_FLAG_BUSY, 1);
        avr_raise_irq(b->irq + IRQ_LCD_BUSY, 1);
        avr_cycle_timer_register_usec(b->avr, delay, lcd_busy_timer, b);
    }
    
    lcd_set_flag(b, LCD_FLAG_REENTRANT, 0);
    return 0;
}

static void lcd_pin_changed_hook(avr_irq_t *irq, uint32_t value, void *param) {
    lcd_t *b = (lcd_t *) param;


    switch (irq->irq) {
        case IRQ_LCD_D0:
        case IRQ_LCD_D1:
        case IRQ_LCD_D2:
        case IRQ_LCD_D3:
        case IRQ_LCD_D4:
        case IRQ_LCD_D5:
        case IRQ_LCD_D6:
        case IRQ_LCD_D7: if (lcd_get_flag(b, LCD_FLAG_REENTRANT)) return;
    }

    uint16_t old = b->pinstate;
    b->pinstate = (b->pinstate & ~(1u << irq->irq)) | (value << irq->irq);
    bool eo = old & (1u << (uint8_t) IRQ_LCD_E);
    bool e = b->pinstate & (1u << (uint8_t) IRQ_LCD_E);
    // only E pin rising edge
    if (!eo && e) avr_cycle_timer_register(b->avr, 1, lcd_process_input, b);
}

static const char *irq_names[IRQ_LCD_COUNT] = {
        [IRQ_LCD_ALL] = "7=lcd.pins",
        [IRQ_LCD_RS] = ">lcd.RS",
        [IRQ_LCD_RW] = ">lcd.RW",
        [IRQ_LCD_E] = ">lcd.E",
        [IRQ_LCD_D0] = "=lcd.D0",
        [IRQ_LCD_D1] = "=lcd.D1",
        [IRQ_LCD_D2] = "=lcd.D2",
        [IRQ_LCD_D3] = "=lcd.D3",
        [IRQ_LCD_D4] = "=lcd.D4",
        [IRQ_LCD_D5] = "=lcd.D5",
        [IRQ_LCD_D6] = "=lcd.D6",
        [IRQ_LCD_D7] = "=lcd.D7",

        [IRQ_LCD_BUSY] = "<lcd.BUSY",
        [IRQ_LCD_ADDR] = "7<lcd.ADDR",
        [IRQ_LCD_DATA_IN] = "8<lcd.DATA_IN",
        [IRQ_LCD_DATA_OUT] = "8<lcd.DATA_OUT"
};

void lcd_init(lcd_t *b, avr_t *avr, int cols, int rows) {
    memset(b, 0, sizeof(*b));
    b->avr = avr;
    b->cols = cols;
    b->rows = rows;

    b->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_LCD_COUNT, irq_names);
    for (int i = 0; i < IRQ_LCD_INPUT_COUNT; i++)
        avr_irq_register_notify(b->irq + i, lcd_pin_changed_hook, b);

    lcd_clear_screen(b);
    lcd_reset_cursor(b);
}

static void avr_biconnect_irq(avr_irq_t *a, avr_irq_t *b) {
    avr_connect_irq(a, b);
    avr_connect_irq(b, a);
}

void lcd_connect4(lcd_t *b, avr_irq_t *D4, avr_irq_t *D5, avr_irq_t *D6, avr_irq_t *D7, avr_irq_t *RS,
                  avr_irq_t *E, avr_irq_t *RW) {
    avr_biconnect_irq(D4, b->irq + IRQ_LCD_D4);
    avr_biconnect_irq(D5, b->irq + IRQ_LCD_D5);
    avr_biconnect_irq(D6, b->irq + IRQ_LCD_D6);
    avr_biconnect_irq(D7, b->irq + IRQ_LCD_D7);
    avr_connect_irq(RS, b->irq + IRQ_LCD_RS);
    avr_connect_irq(E, b->irq + IRQ_LCD_E);
    avr_connect_irq(RW, b->irq + IRQ_LCD_RW);
}
void lcd_connect8(lcd_t *b, avr_irq_t *D0, avr_irq_t *D1, avr_irq_t *D2, avr_irq_t *D3, avr_irq_t *D4,
                  avr_irq_t *D5, avr_irq_t *D6, avr_irq_t *D7, avr_irq_t *RS, avr_irq_t *E, avr_irq_t *RW) {
    avr_biconnect_irq(D0, b->irq + IRQ_LCD_D0);
    avr_biconnect_irq(D1, b->irq + IRQ_LCD_D1);
    avr_biconnect_irq(D2, b->irq + IRQ_LCD_D2);
    avr_biconnect_irq(D3, b->irq + IRQ_LCD_D3);
    lcd_connect4(b, D4, D5, D6, D7, RS, E, RW);
}
bool lcd_isCursorOn(lcd_t *b) {
    return lcd_get_flag(b, LCD_FLAG_C);
}
lcd_cursor_state lcd_cursorState(lcd_t *b) {
    return lcd_get_flag(b, LCD_FLAG_C)
        ? (lcd_get_flag(b, LCD_FLAG_B)
            ? LCD_CURSOR_BLINK
            : LCD_CURSOR_ON)
        : LCD_CURSOR_OFF;
}

